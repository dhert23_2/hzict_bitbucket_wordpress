# Welkom op de repository van HZICT_Wordpress #

In het project vindt je onder andere een bestand readme . md. Dit bestand kun je gebruiken om toelichting te geven op de repository. 

### Wat is readme . md? ###

Readme.md is een zogenaamd markdown bestand. Het is niets meer dan een tekstbestand met een speciale opmaak. Deze opmaak kan weer vertaald worden in een goed leesbaar HTML formaat. Op deze manier kun je snel en gemakkelijk informatie toevoegen aan je repository.

### TIP ###

Neem een kijkje op http://www.dillinger.io. Deze gratis online tool maakt het zeer eenvoudig om een markdown bestand te maken.